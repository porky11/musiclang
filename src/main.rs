use std::{
    collections::HashMap,
};

use ssexp::{List, Symbol, Token};

fn to_frequency(midi: i8) -> f32 {
    2.0f32.powf((midi - 69) as f32 / 12.0) * 440.0
}

fn to_rel_frequency(midi: i8) -> f32 {
    2.0f32.powf(midi as f32 / 12.0)
}

fn to_midi(frequency: f32) -> i8 {
    ((frequency / 440.0).log2() * 12.0).round() as i8 + 69
}

type Tone = Box<Play>;

fn collect_tones<T: IntoIterator<Item = Token>>(
    tones: &mut Vec<Tone>,
    tokens: T,
    vars: &mut HashMap<String, Vec<Token>>,
) {
    for token in tokens {
        collect_tone(tones, token, vars)
    }
}


fn collect_tone(tones: &mut Vec<Tone>, token: Token, vars: &mut HashMap<String, Vec<Token>>) {
    match token {
        Symbol(string) => {
            if let Ok(tone) = string.parse::<i8>() {
                tones.push(Box::new(to_rel_frequency(tone)))
            } else if let Ok(freq) = string.parse::<f32>() {
                tones.push(Box::new(freq))
            } else {
                let tokens = vars.get(&string).expect("Invalid tone").clone();
                collect_tones(tones, tokens, vars);
            }
        }
        List(vec) => {
            let mut tokens = vec.into_iter();
            let kind = tokens
                .next()
                .unwrap()
                .symbol()
                .expect("First element has to be a Symbol");
            match kind.as_str() {
                "sine" => {
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Sine {
                        data: data.pop().unwrap(),
                    }));
                }
                "block" => {
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Block {
                        data: data.pop().unwrap(),
                    }));
                }
                "inv" => {
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Inv {
                        data: data.pop().unwrap(),
                    }));
                }
                "pos" => {
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Pos {
                        data: data.pop().unwrap(),
                    }));
                }
                "rep" => {
                    let times = tokens
                        .next()
                        .unwrap()
                        .symbol()
                        .expect("Repetitions have to be a symbol")
                        .parse::<usize>()
                        .unwrap();
                    let num = 0;
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Rep {
                        num,
                        times,
                        data: data.pop().unwrap(),
                    }));
                }
                "var" => {
                    vars.insert(
                        tokens
                            .next()
                            .unwrap()
                            .symbol()
                            .expect("Variable names have to be symbols"),
                        tokens.collect(),
                    );
                }
                "ign" => {}
                "sim" => {
                    tones.push(Box::new(Play::new()));
                }
                "amp" => {
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Amp { data }));
                }
                "seq" => {
                    let num = 0;
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Seq { num, data }));
                }
                "com" => {
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Com { data }));
                }
                "len" => {
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Len {
                        state: 0.0,
                        data: data.pop().unwrap(),
                        len: data.pop().unwrap(),
                    }));
                }
                "freq" => {
                    let mut data = Vec::new();
                    collect_tones(&mut data, tokens, vars);
                    tones.push(Box::new(Freq { data }));
                }
                "tone" => {}
                string => panic!("Operation `{}` not supported", string),
            }
        }
    }
}

trait Play: 'static + Send {
    fn play(&mut self, speed: f32) -> f32;
    fn is_play(&mut self) -> bool {
        true
    }
    /*
    fn tone(self, tone: i8) -> Freq where Self: Sized {
        let freq = to_rel_frequency(tone);
        Freq {
            data: vec![Box::new(self),Box::new(freq)]
        }
    }
    fn freq<T>(self, freq: T) -> Freq where Self: Sized, T: Play+Sized {
        Freq {
            data: vec![Box::new(self),Box::new(freq)]
        }
    }
    fn amp<T>(self, other: T) -> Amp where Self: Sized, T: Play+Sized {
        Amp {
            data: vec![Box::new(self), Box::new(other)],
        }
    }
    fn sine(self) -> Sine where Self: Sized {
        Sine {
            data: Box::new(self)
        }
    }
    fn block(self) -> Block where Self: Sized {
        Block {
            data: Box::new(self)
        }
    }
    fn pos(self) -> Pos where Self: Sized {
        Pos {
            data: Box::new(self)
        }
    }
    fn inv(self) -> Inv where Self: Sized {
        Inv {
            data: Box::new(self)
        }
    }
    fn com<T>(self, other: T) -> Com where Self: Sized, T: Play+Sized {
        Com {
            data: vec![Box::new(self), Box::new(other)],
        }
    }
    fn seq<T,U>(self, other: T, len: U) -> Seq where Self: Sized, T: Play+Sized, U: Play+Sized {
        Seq {
            num: 0,
            data: vec![Box::new(self), Box::new(other)],
        }
    }*/
}

impl Play {
    fn new() -> Simple {
        Simple { state: 0.0 }
    }
    /*
    fn from_freq<T>(freq: T) -> Freq where T: Play+Sized {
        Freq {
            data: vec![Box::new(Self::new()),Box::new(freq)]
        }
    }
    fn from_tone(tone: i8) -> Freq {
        let freq = to_frequency(tone);
        Freq {
            data: vec![Box::new(Self::new()),Box::new(freq)]
        }
    }*/
}

struct Simple {
    state: f32,
}

impl Play for Simple {
    fn play(&mut self, speed: f32) -> f32 {
        self.state += 1.0;
        self.state /= 2.0;
        self.state += speed;
        self.state %= 1.0;
        self.state *= 2.0;
        self.state -= 1.0;

        self.state
    }
}

impl Play for f32 {
    fn play(&mut self, _: f32) -> f32 {
        *self
    }
}

struct Sine {
    data: Box<Play>,
}

impl Play for Sine {
    fn play(&mut self, speed: f32) -> f32 {
        (self.data.play(speed) * std::f32::consts::PI).sin() / 2.0 + 0.5
    }
    fn is_play(&mut self) -> bool {
        self.data.is_play()
    }
}

struct Block {
    data: Box<Play>,
}

impl Play for Block {
    fn play(&mut self, speed: f32) -> f32 {
        self.data.play(speed).signum()
    }
    fn is_play(&mut self) -> bool {
        self.data.is_play()
    }
}

struct Inv {
    data: Box<Play>,
}

impl Play for Inv {
    fn play(&mut self, speed: f32) -> f32 {
        1.0 - self.data.play(speed)
    }
    fn is_play(&mut self) -> bool {
        self.data.is_play()
    }
}

struct Pos {
    data: Box<Play>,
}

impl Play for Pos {
    fn play(&mut self, speed: f32) -> f32 {
        self.data.play(speed).abs()
    }
    fn is_play(&mut self) -> bool {
        self.data.is_play()
    }
}

struct Com {
    data: Vec<Box<Play>>,
}

impl Play for Com {
    fn play(&mut self, speed: f32) -> f32 {
        self.data.iter_mut().map(|obj| obj.play(speed)).sum()
    }
    /*
    fn com<T>(self, other: T) -> Com where Self: Sized, T: Play+Sized {
        let mut com = self;
        com.data.push(Box::new(other));
        com
    }*/
}

struct Len {
    state: f32,
    len: Box<Play>,
    data: Box<Play>,
}

impl Play for Len {
    fn play(&mut self, speed: f32) -> f32 {
        if !(self.state < 1.0) {
            panic!("Tone finished")
        }
        self.state += speed / self.len.play(speed);
        self.data.play(speed)
    }
    fn is_play(&mut self) -> bool {
        self.state < 1.0 || {
            self.state -= 1.0;
            false
        }
    }
}

struct Seq {
    num: usize,
    data: Vec<Box<Play>>,
}

impl Play for Seq {
    fn play(&mut self, speed: f32) -> f32 {
        while !self.data[self.num].is_play() {
            self.num += 1;
        }
        self.data[self.num].play(speed)
    }
    fn is_play(&mut self) -> bool {
        if self.num == self.data.len() - 1 {
            self.data[self.num].is_play() || {
                self.num = 0;
                false
            }
        } else {
            true
        }
    }
}

struct Rep {
    num: usize,
    times: usize,
    data: Box<Play>,
}

impl Play for Rep {
    fn play(&mut self, speed: f32) -> f32 {
        while !self.data.is_play() {
            self.num += 1;
        }
        if self.num == self.times {
            panic!("Tone finished")
        }
        self.data.play(speed)
    }
    fn is_play(&mut self) -> bool {
        if self.num == self.times - 1 {
            self.data.is_play() || {
                self.num = 0;
                false
            }
        } else {
            true
        }
    }
}

struct Amp {
    data: Vec<Box<Play>>,
}

impl Play for Amp {
    fn play(&mut self, speed: f32) -> f32 {
        self.data.iter_mut().map(|obj| obj.play(speed)).product()
    }
    fn is_play(&mut self) -> bool {
        self.data.iter_mut().all(|obj| obj.is_play())
    }
    /*
    fn amp<T>(self, other: T) -> Amp where Self: Sized, T: Play+Sized {
        let mut amp = self;
        amp.data.push(Box::new(other));
        amp
    }*/
}

struct Freq {
    data: Vec<Box<Play>>,
}

impl Play for Freq {
    fn play(&mut self, speed: f32) -> f32 {
        self.data
            .iter_mut()
            .rev()
            .fold(1.0, |other, obj| obj.play(speed * other))
    }
}

fn main() {
    let device = cpal::default_output_device().expect("Failed to get default output device");
    let format = device
        .default_output_format()
        .expect("Failed to get default output format");
    let event_loop = cpal::EventLoop::new();
    let stream_id = event_loop.build_output_stream(&device, &format).unwrap();
    event_loop.play_stream(stream_id.clone());

    let sample_rate = format.sample_rate.0 as f32;
    let _sample_clock = 0f32;

    println!("{}", to_rel_frequency(0));
    println!("{}", to_frequency(0));

    let tokens = {
        use ssexp::{functions::parse_list_inline, parse_sexp, MacroMap};
        use std::{
            fs::File,
            io::prelude::*,
            env::args,
        };
        
        let mut args = args();
        args.next();
        let path = args.next().expect("Path argument required");

        let mut file = File::open(path).expect("Unable to open the file");
        let mut text = String::new();
        file.read_to_string(&mut text)
            .expect("Unable to read the file");

        let map = MacroMap::new()
            .with_lists('(')
            .with_comments(';')
            .with_seperating_whitespaces();

        parse_sexp(text.chars(), parse_list_inline, map)
    };

    let mut data = Vec::new();

    let mut vars = HashMap::new();

    collect_tones(&mut data, tokens, &mut vars);

    let mut play = Com { data };
    /*
    let mut play = Play::from_tone(60).sine().freq(
        Play::from_freq(
            Play::from_freq(0.25).sine().pos().amp(32.0).combine(
                Play::from_freq(0.5).block().amp(32.0)
            )
        ).sine().combine(
            Play::from_freq(1.0).amp(4.0).inv()
        ).freq(1.0.seq(2.0,1.0))
    ).amp(0.025).combine(
        Play::from_tone(48).inv().freq(
            Play::from_freq(0.125).sine().amp(16.0)
        ).amp(0.025).freq(0.5.seq(2.0,2.0).seq(1.0,1.0).seq(1.2,4.0))
    );

    let mut play = Play::from_tone(60).sine().amp(0.05).seq(
        Play::from_tone(65).block().amp(0.01),1.0f32.block()
    );
    /*.seq(
        Play::from_tone(65).sine(), 20.0
    ).amp(0.05);*/
    */
    let mut next_value = move || play.play(1.0 / sample_rate) * 2.0 - 1.0;

    event_loop.run(move |_, data| -> () {
        match data {
            cpal::StreamData::Output {
                buffer: cpal::UnknownTypeOutputBuffer::U16(mut buffer),
            } => {
                for sample in buffer.chunks_mut(format.channels as usize) {
                    let value = ((next_value() * 0.5 + 0.5) * std::u16::MAX as f32) as u16;
                    for out in sample.iter_mut() {
                        *out = value;
                    }
                }
            }
            cpal::StreamData::Output {
                buffer: cpal::UnknownTypeOutputBuffer::I16(mut buffer),
            } => {
                for sample in buffer.chunks_mut(format.channels as usize) {
                    let value = (next_value() * std::i16::MAX as f32) as i16;
                    for out in sample.iter_mut() {
                        *out = value;
                    }
                }
            }
            cpal::StreamData::Output {
                buffer: cpal::UnknownTypeOutputBuffer::F32(mut buffer),
            } => {
                for sample in buffer.chunks_mut(format.channels as usize) {
                    let value = next_value();
                    for out in sample.iter_mut() {
                        *out = value;
                    }
                }
            }
            _ => (),
        }
    });
}
